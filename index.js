const express = require('express');
const app = express();
const server = require('http').createServer(app);
const io = require('socket.io')(server);
const port = process.env.PORT || 3000;

server.listen(port, function() {
    console.log('Server listening at', port);
});

app.get('/', function(req, res) {
    res.sendFile(__dirname + '/index.html');
});

var numUsers = 0;

io.on('connection', function(socket) {
    var addedUser = false;

    socket.on('new message', function(data) {
        io.sockets.emit('messageToAll', {
            username: data.username,
            message: data.message
        });
    });

    socket.on('add user', function(username){
        if (addedUser) return;

        socket.username = username;
        ++numUsers;
        addedUser = true;
        socket.emit('login', numUsers);
        socket.broadcast.emit('user joined', {
            username: socket.username,
            numUsers: numUsers
        })
    });

    socket.on('disconnected', function(username){
        if (addedUser) {
            --numUsers;
            socket.broadcast.emit('user left', {
                username: username,
                numUsers: numUsers
            });
        }
    });
});